window.onscroll = function() {isTheTop()};

function isTheTop() {
		var headerElement = document.getElementById('nav');
		var headerHeight =  headerElement.offsetHeight;

    if (document.body.scrollTop || document.documentElement.scrollTop > headerHeight) {
					headerElement.style.opacity='0.5';
					
					headerElement.onmouseover = function() {
						headerElement.style.opacity='1';
					}
					headerElement.onmouseout = function() {
						headerElement.style.opacity='0.5';
					}
    } else {
					headerElement.style.opacity='0';
					headerElement.onmouseout = function() {
						headerElement.style.opacity='0';
					}
    }
}